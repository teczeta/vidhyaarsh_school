<footer class="bg-soft-primary">
    <div class="container">
        <div class="card shadow-lg mt-n16 mt-md-n21 mb-15 mb-md-14">
            <div class="row gx-0">
                <div class="col-lg-6 image-wrapper bg-image bg-cover rounded-top rounded-lg-start d-none d-md-block"
                     data-image-src="assets/img/contact.png">
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <div class="p-10 p-md-11 p-lg-13">
                        <h2 class="display-4 mb-3">Connect With Us</h2>
                        <p class="lead fs-lg">Reach Out, Discover, and Connect with Vidhyaarsh Public School</p>
                        <p>We would love to hear from you! Whether you have questions, feedback, or would like to
                            schedule a visit to experience our vibrant learning community firsthand, getting in touch
                            with us is easy. Our dedicated team is here to provide you with the information and support
                            you need. </p>
                        <a href="{{route('contact')}}" class="btn btn-primary rounded-pill mt-2">Get in Touch</a>
                    </div>
                    <!--/div -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.container -->
    <div class="container pb-12 text-center">
        <div class="row mt-n10 mt-lg-0">
            <div class="col-xl-10 mx-auto">
                <!--/.row -->
                <p>© 2023 Teczeta. All rights reserved.</p>
                <nav class="nav social justify-content-center">
                    <a href="#"><i class="uil uil-twitter"></i></a>
                    <a href="#"><i class="uil uil-facebook-f"></i></a>
                    <a href="#"><i class="uil uil-instagram"></i></a>
                    <a href="#"><i class="uil uil-youtube"></i></a>
                </nav>
                <!-- /.social -->
            </div>
            <!-- /column -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>
