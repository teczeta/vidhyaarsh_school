<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vidhyaarsh Public School</title>
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/colors/violet.css">
    <link rel="preload" href="assets/css/fonts/dm.css" as="style" onload="this.rel='stylesheet'">
    <style>
        .caption {
            text-align: left;
            caption-side: top;
            color: black;
            font-size: 20px;
            padding-bottom: 10px;
            text-decoration: underline;
        }

    </style>
</head>

<body>
<div class="content-wrapper">
    @include('includes.header')
    @yield('content')
</div>
<!-- /.content-wrapper -->
@include('includes.footer')
<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
    </svg>
</div>
<script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/theme.js"></script>
</body>
</html>
