@extends('layouts.app')

@section('content')
    <section class="wrapper bg-gray">
        <div class="container py-10 pt-md-14 text-center">
            <div class="row">
                <div class="col-xl-6 mx-auto">
                    <h1 class="display-1 mb-4">Hello! We are Vidhyaarsh Public School</h1>
                    <p class="lead fs-lg mb-0">A Place where future is born.</p>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-soft-primary angled upper-end lower-end">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 mb-14 mb-md-17 align-items-center">
                <div class="col-lg-6 position-relative order-lg-2">
                    <div class="shape bg-dot primary rellax w-16 h-20" data-rellax-speed="1"
                         style="top: 3rem; left: 5.5rem"></div>
                    <div class="overlap-grid overlap-grid-2">
                        <div class="item">
                            <figure class="rounded shadow"><img src="assets/img/1.png" alt="">
                            </figure>
                        </div>
                        <div class="item">
                            <figure class="rounded shadow"><img src="assets/img/2.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <img src="assets/img/icons/lineal/megaphone.svg" class="svg-inject icon-svg icon-svg-md mb-4"
                         alt=""/>
                    <h2 class="display-4 mb-3">Who Are We?</h2>
                    <p class="lead fs-lg">
                        Unleash Potential, Embrace Excellence: Discover the Difference
                    </p>
                    <p class="mb-6">
                        Welcome to Vidhyaarsh Public School, where education comes alive with passion, purpose, and
                        endless
                        possibilities. At Vidhyaarsh Public School, we are dedicated to providing an exceptional
                        learning
                        environment that nurtures the potential of every student. Our mission is to empower young minds,
                        instill a love for learning, and cultivate the skills and character needed to thrive in an
                        ever-changing world.
                    </p>

                    <!--/.row -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <div class="row mb-5">
                <div class="col-md-10 col-xl-8 col-xxl-7 mx-auto text-center">
                    <img src="assets/img/icons/lineal/list.svg" class="svg-inject icon-svg icon-svg-md mb-4" alt=""/>
                    <h2 class="display-4 mb-4 px-lg-14">Our Core values</h2>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div class="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
                <div class="col-lg-6 order-lg-2">
                    <div class="card me-lg-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span
                                            class="number">01</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Excellence</h4>
                                    <p class="mb-0">We strive for excellence in all areas of education, encouraging
                                        our students to pursue their passions, set high standards, and achieve their
                                        fullest potential.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                    <div class="card ms-lg-13 mt-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span
                                            class="number">02</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Innovation</h4>
                                    <p class="mb-0">We embrace innovation and creativity, fostering an environment that
                                        encourages critical thinking, problem-solving, and the exploration of new ideas
                                        and technologies.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                    <div class="card mx-lg-6 mt-6">
                        <div class="card-body p-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <span class="icon btn btn-circle btn-lg btn-soft-primary pe-none me-4"><span
                                            class="number">03</span></span>
                                </div>
                                <div>
                                    <h4 class="mb-1">Integrity</h4>
                                    <p class="mb-0">We uphold the highest standards of integrity, fostering honesty,
                                        ethical behavior, and responsibility in all aspects of our school community.</p>
                                </div>
                            </div>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <h2 class="display-6 mb-3">Our Vision</h2>
                    <p>Our mission is to provide high quality education and childcare in a safe, respectful and
                        inclusive environment that builds a foundation for life-long learning.</p>
                    <h2 class="display-6 mb-3">Our Mission</h2>
                    <p class="mb-6">
                        We examine, what, how, why and who they teach. We believe that the goal
                        of the teacher is to develop student’s minds powerful thinkers.
                    </p>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light mb-16 mb-md-22">
        <div class="container py-14 py-md-16">
            <div class="row mb-10">
                <div class="col-xl-10 mx-auto">
                    <div class="row align-items-center counter-wrapper gy-6 text-center">
                        <div class="col-md-3">
                            <img src="assets/img/icons/lineal/check.svg"
                                 class="svg-inject icon-svg icon-svg-lg text-primary mb-3" alt=""/>
                            <h3 class="counter">718</h3>
                            <p>Students</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <img src="assets/img/icons/lineal/user.svg"
                                 class="svg-inject icon-svg icon-svg-lg text-primary mb-3" alt=""/>
                            <h3 class="counter">32</h3>
                            <p>Teachers</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <img src="assets/img/icons/lineal/briefcase-2.svg"
                                 class="svg-inject icon-svg icon-svg-lg text-primary mb-3" alt=""/>
                            <h3 class="counter">14</h3>
                            <p>Classes</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-3">
                            <img src="assets/img/icons/lineal/award-2.svg"
                                 class="svg-inject icon-svg icon-svg-lg text-primary mb-3" alt=""/>
                            <h3 class="counter">24</h3>
                            <p>Awards Won</p>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
