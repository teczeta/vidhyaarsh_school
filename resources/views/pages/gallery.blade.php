@extends('layouts.app')

@section('content')
    php
    <!-- /section -->
    <section class="wrapper bg-light mb-16 mb-md-22">
        <div class="container py-14 py-md-16">
            <div class="grid grid-view projects-masonry">
                {{--<div class="isotope-filter filter mb-10">
                    <p>Filter:</p>
                    <ul>
                        <li><a class="filter-item active" data-filter="*">All</a></li>
                        <li><a class="filter-item" data-filter=".concept">Infrastructure</a></li>
                        <li><a class="filter-item" data-filter=".product">School Ground</a></li>
                        <li><a class="filter-item" data-filter=".workshop">Science Lab</a></li>
                        <li><a class="filter-item" data-filter=".still-life">Computer Lab</a></li>
                    </ul>
                </div>--}}
                <div class="row gx-md-8 gy-10 gy-md-13 isotope">
                    @for($i=1;$i<=59;$i++)
                        <div class="project item col-md-6 col-xl-4">
                            <figure class="lift rounded mb-6"><a href="#"> <img
                                        src="assets/img/gallery/gallery_{{$i}}.jpeg" alt=""/></a></figure>

                            <!-- /.project-details -->
                        </div>
                    @endfor


                    <!-- /.project -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.grid -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
