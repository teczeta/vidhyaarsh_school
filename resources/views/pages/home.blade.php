@extends('layouts.app')

@section('content')
    <section class="wrapper bg-light position-relative min-vh-70 d-lg-flex align-items-center">
        <div
            class="rounded-4-lg-start col-lg-6 order-lg-2 position-lg-absolute top-0 end-0 image-wrapper bg-image bg-cover h-100 min-vh-50"
            data-image-src="assets/img/gallery/cover.jpg">
        </div>
        <!--/column -->
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="mt-10 mt-md-11 mt-lg-n10 px-10 px-md-11 ps-lg-0 pe-lg-13 text-center text-lg-start"
                         data-cues="slideInDown" data-group="page-title" data-delay="600">
                        <h1 class="display-1 mb-5">Igniting Minds, Shaping Destinies: Where Education Thrives.</h1>
                        <p class="lead fs-25 lh-sm mb-7 pe-md-10">
                            Vidhyaarsh Public School
                        </p>
                        <div class="d-flex justify-content-center justify-content-lg-start" data-cues="slideInDown"
                             data-group="page-title-buttons" data-delay="900">
                            <span><a href="{{route('about')}}" class="btn btn-lg btn-primary rounded-pill me-2">Know More</a></span>
                            <span><a href="{{route('contact')}}" class="btn btn-lg btn-outline-primary rounded-pill">Contact Us</a></span>
                        </div>
                    </div>
                    <!--/div -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-18">
            <!--/.row -->
            <div class="row gx-lg-8 gx-xl-12 gy-8">
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/telephone-3.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Ventilation and Air Quality</h4>
                            <p class="mb-0">
                                Big and spacious classrooms provide very good ventilation. More number of fans bring
                                constant stream of fresh air and polluted stale air is entrusted from the room.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/shield.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Classroom Facilities</h4>
                            <p class="mb-0">
                                Our class rooms are designed and equipped with the modern teaching facilities like
                                projectors and Big sized blackboard, Seminar halls to conduct seminars and GD sessions.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/cloud-computing-3.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Study Programme</h4>
                            <p class="mb-0">
                                Our schedule allows the children block out time, to develop strong study habits and
                                co-curricular activities.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/analytics.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Playground and Sports</h4>
                            <p class="mb-0">
                                Huge play grounds to play all types of games suitable for all the age group. We conduct
                                District, Zonal level tournaments in our big play grounds.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/settings.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Library and Lab</h4>
                            <p class="mb-0">
                                A big library with more than 10,000 books is there to inculcate the essential habit of
                                reading in students. Well equipped, spacious labs for Physics, Chemistry, Biology and
                                Computer.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
                <div class="col-md-6 col-lg-4">
                    <div class="d-flex flex-row">
                        <div>
                            <img src="assets/img/icons/lineal/earth.svg"
                                 class="svg-inject icon-svg icon-svg-sm text-primary me-4" alt=""/>
                        </div>
                        <div>
                            <h4 class="mb-1">Digital Learning</h4>
                            <p class="mb-0">
                                We provide cost-effective digital solutions that keep learning relevant and aims to
                                assist learners who face challenges in using online learning platform.
                            </p>
                        </div>
                    </div>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <hr class="my-14 my-md-17"/>
            <div class="row gy-10 gy-sm-13 gx-lg-8 align-items-center">
                <div class="col-lg-7 order-lg-2">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-md-4 offset-md-2 align-self-end">
                            <figure class="rounded"><img src="assets/img/1.png" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 align-self-end">
                            <figure class="rounded"><img src="assets/img/2.png" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6 offset-md-1">
                            <figure class="rounded"><img src="assets/img/3.png" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-4 align-self-start">
                            <figure class="rounded"><img src="assets/img/4.png" alt=""></figure>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                <div class="col-lg-5">
                    <h3 class="display-4 mb-7">Vidhyaarsh Public School</h3>
                    <div class="accordion accordion-wrapper" id="accordionExample">
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingOne">
                                <button class="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne"> Moto of Vidhyaarsh
                                </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                                 data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Love and Serve
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingTwo">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo"> Chairman Message
                                </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                 data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Our school was established with a view of providing quality education based
                                        on counseling to the children belong to economically weaker section.
                                        The Principle of education is to bring grade of knowledge and awareness and to
                                        enjoy life in a well ordered conduct. I am delighted to welcome all the students
                                        to the world of education.
                                        My soul Motto is to give holistic education equivalent to foreign countries in
                                        rural areas.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                        <div class="card plain accordion-item">
                            <div class="card-header" id="headingThree">
                                <button class="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                        aria-expanded="false" aria-controls="collapseThree"> CEO Message
                                </button>
                            </div>
                            <!--/.card-header -->
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                                 data-bs-parent="#accordionExample">
                                <div class="card-body">
                                    <p>
                                        Education enables a person to face new challenges, achieve progresses and lead a
                                        successful life. I do strongly believe in my spirited staff members and
                                        dedicated parents to build up the school as one among the best school in the
                                        region.
                                    </p>
                                </div>
                                <!--/.card-body -->
                            </div>
                            <!--/.accordion-collapse -->
                        </div>
                        <!--/.accordion-item -->
                    </div>
                    <!--/.accordion -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper image-wrapper bg-image bg-overlay" data-image-src="assets/img/bg-banner.png">
        <div class="container py-18 text-center">
            <div class="row">
                <div class="col-lg-10 col-xl-10 col-xxl-8 mx-auto">
                    <h2 class="display-4 px-lg-10 px-xl-13 px-xxl-10 text-white">Find out everything you need to know
                        about our school.</h2>
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container py-14 py-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10 gy-lg-0 mb-11">
                <div class="col-lg-4">
                    <h3 class="display-4 mb-3 pe-xl-10">We are proud of our Students</h3>
                    <p class="lead fs-lg mb-0 pe-xxl-10">
                        Our achievements and facts
                    </p>
                </div>
                <!-- /column -->
                <div class="col-lg-8 mt-lg-2">
                    <div class="row align-items-center counter-wrapper gy-6 text-center">
                        <div class="col-md-4">
                            <img src="assets/img/icons/lineal/check.svg"
                                 class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                            <h3 class="counter">238</h3>
                            <p>Students</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-4">
                            <img src="assets/img/icons/lineal/user.svg"
                                 class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                            <h3 class="counter">12</h3>
                            <p>Classes</p>
                        </div>
                        <!--/column -->
                        <div class="col-md-4">
                            <img src="assets/img/icons/lineal/briefcase-2.svg"
                                 class="svg-inject icon-svg icon-svg-md text-primary mb-3" alt=""/>
                            <h3 class="counter">24</h3>
                            <p>Awards</p>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->

            <hr class="mt-15 mt-md-18 mb-14 mb-md-17"/>
            <!-- /div -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
@endsection
