@extends('layouts.app')

@section('content')
    <section class="wrapper bg-light">
        <div class="container pt-10 pt-md-14">
            <div class="row">
                <div class="col-md-8 col-lg-7 col-xl-6 col-xxl-5">
                    <h1 class="display-1 mb-10">Mandatory Disclosure</h1>
                    <p class="lead fs-lg pe-lg-15 pe-xxl-12"></p>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <section class="wrapper bg-light angled upper-end">
        <div class="container pb-11 mb-16 mb-md-22">
            <div>
                <table class="table table-bordered">
                    <caption class="caption">
                        A. General Information
                    </caption>
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Information</th>
                        <th>Upload Documents</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Name of the school</td>
                        <td>VIDHYAARSH PUBLIC SCHOOL</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Affiliation no. (if applicable)</td>
                        <td>
                            -
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>School code (if applicable)</td>
                        <td>
                            -
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Complete address with pin code</td>
                        <td>
                            NO 3, PERUMAL KOVIL STREET , BATHRIMED,
                            MANGADU ROAD, KUMANANCHAVADI, CHENNAI ,
                            TAMILNADU - 600056
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Principal name &amp; qualification</td>
                        <td>Dr. V PADMAVATHY - M.Sc, M.Ed, MBA, M.S, Ph.d.</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>School email id</td>
                        <td>vidhyaarsh2023@gmail.com</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Contact details (mobile and landline)</td>
                        <td>7299006568</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>School Website</td>
                        <td>-</td>
                    </tr>

                    </tbody>
                </table>
                <hr class="my-5">
            </div>
            <div>
                <table class="table table-bordered">
                    <caption class="caption">
                        B. Documents and Information
                    </caption>
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Documents/Information</th>
                        <th>Upload Documents</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Copies of affiliation/ upgradation letter and recent extension of affiliation, if any</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td> Copies of societies/ trust/ company registration/ renewal certificate, as applicable</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Copy of the no objection certificate (noc) issued, if applicable, by the state govt./ ut
                        </td>
                        <td><a target="_blank" download="" href="{{asset('docs/FIRE CER.pdf')}}">Click Here</a></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Copies of recognition certificate under rte act, 2009, and its renewal if applicable</td>
                        <td><a target="_blank" download="" href="{{asset('docs/water.pdf')}}">Click Here</a></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Copy of Land Document</td>
                        <td><a target="_blank" download="" href="{{asset('docs/CERTIFICATE OF LAND VIDHYAARSH.pdf')}}">Click
                                Here</a></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Copy of valid fire safety certificate issued by the competent authority</td>
                        <td><a target="_blank" download="" href="{{asset('docs/FIRE CER.pdf')}}">Click Here</a></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Copy of valid building safety certificate as per the national building code</td>
                        <td><a target="_blank" download="" href="{{asset('docs/building.pdf')}}">Click Here</a></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Copies of valid water, health and sanitation certificates</td>
                        <td><a target="_blank" download="" href="{{asset('docs/water.pdf')}}">Click Here</a></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Copy of Lease Document</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Copies of self affidavit</td>
                        <td><a target="_blank" download="" href="{{asset('docs/GetSelfCertificationPdf.pdf')}}">Click
                                Here</a></td>
                    </tr>


                    </tbody>
                </table>
                <hr class="my-5">
            </div>
            <div>
                <table class="table table-bordered">
                    <caption class="caption">
                        C. Results &amp; Academics
                    </caption>
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Documents/Information</th>
                        <th>Upload Documents</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Fee structure of the school</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td> Annual academic calendar</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Last three-year result of the board examination as per applicability</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>List of school management committee (SMC)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>List of parents teachers association (PTA) members</td>
                        <td></td>
                    </tr>


                    </tbody>
                </table>
                <hr class="my-5">
            </div>
            <div>
                <table class="table table-bordered">
                    <caption class="caption">
                        D. Staff and Teaching
                    </caption>
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Information</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Principal</td>
                        <td> Dr. V PADMAVATHY</td>
                    </tr>
                    <tr>
                        <td rowspan="4">2</td>
                        <td> Number of teachers</td>
                        <td>16</td>
                    </tr>

                    <tr>
                        <td>I. PGT</td>
                        <td>00</td>
                    </tr>
                    <tr>

                        <td>II. PRT</td>
                        <td>06</td>
                    </tr>
                    <tr>

                        <td>II. PRT</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td> Teacher/ section ratio</td>
                        <td>1:4</td>
                    </tr>

                    <tr>
                        <td>4</td>
                        <td> Special educator details</td>
                        <td>PADMAVATHY</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td> Counsellor and wellness teacher details</td>
                        <td>MRS ANJALY</td>
                    </tr>
                    </tbody>
                </table>
                <hr class="my-5">
            </div>
            <div>
                <table class="table table-bordered">
                    <caption class="caption">
                        E. School Infrastructure
                    </caption>
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Information</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Total campus area of the school (in square mtr.)</td>
                        <td>6082</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td> Number and size of class rooms (in square mtr.)</td>
                        <td>64 & 47</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Number of large labs including computer labs (in square mtr.)</td>
                        <td>03 & 68</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Internet facility (y/n)</td>
                        <td>Yes</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Number of girl's toilets</td>
                        <td>45</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Number of boy's toilets</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Youtube video link of school's inspection</td>
                        <td></td>
                    </tr>


                    </tbody>
                </table>

            </div>
        </div>
        <!-- /.container -->
    </section>
@endsection
